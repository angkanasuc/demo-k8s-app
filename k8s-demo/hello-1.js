const express = require('express')
const os = require('os');
const app = express()

app.get('/', function (req, res) {
	res.send('Hello World! ' + String(os.hostname()))
})

app.listen(8000, () => console.log('Hello World! listening on port 8000!'))